[bits 16]
[org 0x7c00]

kernel_offset equ 0x1000

mov bp, 0x9000 ; Set stack in 0x9000
mov sp, bp

call clear_screen

mov [boot_drive], dl ; dl starts with the value of which disk is booted

mov bx, loading_output
call print_string ; printing loading...

mov al, 0xf
mov dl, [boot_drive]
mov bx, kernel_offset
call load_from_disk ; Loading kernel from disk to ram add address kernel_offset

mov bx, switching_output
call print_string 

jmp switch_to_32bit

jmp $

; input: al: number of sectors aka size of kernel/512bytes
;        dl: drive selected
;        bx: Address selected
[bits 16]
load_from_disk:
    pusha
    ; al is the kernel size in sectors

    ; ah: 0x2 -> Read operation
    ; al -> Number of sectors to read
    ; ch -> cylinder num
    ; cl -> sector start
    ; dh -> head num
    ; dl -> first floppy
    ; returns
    ; data goes into es:bx
    ; cf -> if set, error else works
    ; ah -> return code
    ; al -> number of sectors successfully read

    ; set 0x7e0:0 address
    ; mov ax, 0x7e0
    ; mov es, ax
    ; mov bx, 0x0

    ; calls interrupt and gives it all data necessairy
    mov ah, 0x2
    mov ch, 0x0
    mov cl, 0x2
    mov dh, 0x0
    int 0x13
    
    jc print_disk_error_flag
    cmp al, 0xf
    jne print_disk_error_cmp
    jmp no_disk_error

    print_disk_error_cmp:
        mov bx, disk_error_not_enought_segments
        call print_string
        jmp $


    print_disk_error_flag:
        mov bx, disk_error_error_flag
        call print_string
        
        call convert_to_hex ; convert ax to hex string called hex_num

        mov bx, hex_num
        call print_string
        
        mov ax, dx

        call convert_to_hex ; convert ax to hex string called hex_num

        mov bx, endl
        call print_string

        mov bx, hex_num
        call print_string

        jmp $

    no_disk_error:
    ; jmp $
    popa
    ret

; print text in bios
; bx is address of string(pointer to first char)
[bits 16]
print_string:
    pusha
    mov ah, 0xe
    print_loop:
        mov al, [bx]
        cmp al, 0
        je print_done
        int 0x10
        inc bx
        jmp print_loop
    print_done:
    popa
    ret

; ax -> hex num
; changes hex_num string to reflect the number in ax
[bits 16]
convert_to_hex:
    pusha
    mov cx, 0 ; cx is counter
    hex_loop:
        ; grab lowest 4bits of num
        mov dx, 0xf 
        and dx, ax
        shr ax, 4
        
        ; select right char to edit
        mov bx, hex_num
        add bx, 5
        sub bx, cx
        inc cx

        cmp dx, 0xA
        jl normal_num
        jge letter_num
        final_cmp_convert_to_hex:
        cmp cx, 4
        je hex_done
        jmp hex_loop

        normal_num:
            add dx, 48
            mov byte [bx], dl
            jmp final_cmp_convert_to_hex
            
        letter_num:
            add dx, 55
            mov byte [bx], dl
            jmp final_cmp_convert_to_hex

    hex_done:
    popa
    ret

hex_num:
    db "0x0000", 0

; print text in protected mode
; // al/ah is start position(row/column)
; ecx is address of string(pointer to first char)
[bits 32]
print_string_pm:
    pusha
    mov edx, VIDEO_ADDRESS

    ; mov ebx, al ; Set start position
    ; mul ebx, 160 ; Set start position
    ; add ebx, ah ; Set start position
    
    mov ebx, 0 ; start at index 0
    add edx, ebx ; set start address

    print_loop_32:
        mov al, [ecx]  
        mov ah, 0xf ; 0xf for color white on black
        cmp al, 0 ; if char is 0, the printing is done
        je print_done_32
        mov [edx], ax
        add ecx, 1
        add edx, 2
        jmp print_loop_32
    print_done_32:
    popa
    ret

[bits 16]
clear_screen:
    pusha
    mov ah, 06 
    mov al, 00 ; set al to 0 to clear all page
    mov bh, 07 ; attribute to write blank lines in the bottom???
    mov cx, 0  ; row/column of window s upper left corner
    mov dh, 24 ; row lower right corner
    mov dl, 79 ; row lower right corner
    int 0x10
    popa
    ret

[bits 16]
switch_to_32bit:
    cli
    lgdt [gdt_descriptor]
    mov eax, cr0
    or eax, 1
    mov cr0, eax
    jmp CODE_SEG:init_32

[bits 32]
init_32:
    mov ax, DATA_SEG
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    mov ebp, 0x9000
    mov esp, ebp
    jmp begin_pm

[bits 32]
begin_pm:
    mov ecx, in_pm_output
    call print_string_pm
    call CODE_SEG:kernel_offset
    jmp $

; -------- GLOBAL VARIABLES ----------

VIDEO_ADDRESS equ 0xb8000

disk_error_not_enought_segments:
    db "Disk Error: Not enought segments loaded", 10, 13, 0

disk_error_error_flag:
    db "Disk Error: Error flag turned on", 10, 13, 0

loading_output:
    db "Loading...", 10, 13, 0

switching_output:
    db "Switching to 32bit....", 10, 13, 0

in_pm_output:
    db "In protected mode....", 0

boot_drive:
    db 0

endl:
    db 10, 13, 0

; GDT 
gdt_start:
    
gdt_null: ; mandatory null descriptor in gdt
    dd 0x0 ; dd means define double word
    dd 0x0

gdt_code: ; code segment descriptor
    ; base(32) = 0x0
    ; limit(20) = 0xfffff
    ; present(1) = 1 (present means segment is present in memory)
    ; privilege(2) = 0 (means ring 0 (highest level))
    ; descriptor type(1) = 1 for code or data, 0 for traps
    ; type(4):
        ; code(1) = 1(means the segment is code)
        ; conforming(1) = 0 (if conforming, code with lower priviledge can access this memory area)
        ; readable(1) = 1 (means the ram can be used for data too instead of just code to execute)
        ; accessed(1) = 0 (this bit is changed by the cpu when we access this segment)
    ; granularity(1) = 1 (if this bit is set, multiplies limit by 16*16*16(4096))
    ; 32_bit default(1) = 1(this bit sets the default data size to 32bits)
    ; 64_bit segment(1) = 0(this bit sets the code to 64 bit)
    ; avl(1): 0 used for debugging but not used in example
    dw 0xffff ; limit first 16 bits
    dw 0x0000 ; base first 16 bits
    db 0x00 ; base next 8 bits
    db 10011010b ; flags present, privilege, descriptor type, type(code, conforming, readable, accessed)
    db 11001111b ; flags granularity, default 32_bit, 64 bit, avl, segment limit next 4 bits
    db 0x0
    
gdt_data:; data segment descriptor
    ; same as gdt_code except the type
    ; type(4):
        ; code(1) = 0
        ; conforming(1) = 0
        ; readable(1) = 1
        ; accessed = 0
    dw 0xffff ; limit first 16 bits
    dw 0x0000 ; base first 16 bits
    db 0x00 ; base next 8 bits
    db 10010010b ; flags present, privilege, descriptor type, type(code, conforming, readable, accessed)
    db 11001111b ; flags granularity, default 32_bit, 64 bit, avl, segment limit next 4 bits
    db 0x0

gdt_end:
    ; the reason to put this tag is to help calculate the length of the code from gdt_start and gdt_end

gdt_descriptor:
    dw gdt_start - gdt_end - 1 ; the size of gdt is start - end - 1
    dd gdt_start ; write pointer to gdt data
; Constants that will be helpful later
CODE_SEG equ gdt_code - gdt_start
DATA_SEG equ gdt_data - gdt_start


times 510 - ($ - $$) db 0
dw 0xaa55

; -----END of 512 bytes-------
