ASM = nasm
BUILD_DIR = ./build
BOOTLOADER_SRC = ./src/bootloader
KERNEL_SRC = ./src/kernel

.PHONY clean all bootloader kernel disk

all: bootloader kernel

bootloader: disk
	make $(BOOTLOADER_SRC)

kernel: disk
	make $(KERNEL_SRC)

disk: $(BUILD_DIR)/disk.img

$(BUILD_DIR)/disk.img:
	dd if=/dev/zero of=$(BUILD_DIR)/disk.img bs=512 count=2880
	mkfs.fat -F 12 $(BUILD_DIR)/disk.img

clean:
	rm $(BUILD_DIR)/*

